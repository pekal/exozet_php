<?php
namespace AppBundle\Client;

interface Client
{
    public function userInformation(String $user);
    public function repositoryList(String $user);
    public function organizationList(String $user);
    public function languageList(String $user, String $repositoryName);
}
