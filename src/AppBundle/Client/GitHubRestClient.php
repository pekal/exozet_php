<?php
namespace AppBundle\Client;

use AppBundle\Exception\BasicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unirest\Request;

class GitHubRestClient implements Client
{
    /**
     * @var String
     */
    private $serviceURL;
    /**
     * @var String
     */
    private $clientId;
    /**
     * @var String
     */
    private $clientSecret;

    /**
     * GitHubRestClient constructor.
     * @param String $serviceURL
     * @param String $clientId
     * @param String $clientSecret
     */
    public function __construct($serviceURL, $clientId, $clientSecret)
    {
        $this->serviceURL = $serviceURL;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function userInformation(String $user)
    {
        $url = $this->serviceURL . "users/" . $user . "?"  . $this->accessParameters();
        return $this->createRequest($url);
    }

    public function repositoryList(String $user)
    {
        $url = $this->serviceURL . "users/" . $user . "/repos?sort=updated&"  . $this->accessParameters();
        return $this->createRequest($url);
    }

    public function organizationList(String $user)
    {
        $url = $this->serviceURL . "users/" . $user . "/orgs?"  . $this->accessParameters();
        return $this->createRequest($url);
    }

    public function languageList(String $user, String $repositoryName)
    {
        $url = $this->serviceURL . "repos/" . $user . "/" . $repositoryName . "/languages?"  . $this->accessParameters();
        return $this->createRequest($url);
    }

    private function accessParameters() : String {
        return 'client_id=' . $this->clientId . '&client_secret=' . $this->clientSecret;
    }

    private function createRequest(String $url) {
        $headers = ['Accept' => 'application/json'];
        $request = Request::get($url, $headers);
        if ($request->code != JsonResponse::HTTP_OK) {
            throw new BasicException('Problem with get data from REST API', $request->code);
        }
        return $request->body;
    }
}
