<?php

namespace AppBundle\Controller;

use AppBundle\Exception\BasicException;
use AppBundle\Form\MainForm;
use AppBundle\Model\MainFormEntity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $mainFormEntity = new MainFormEntity();
        $form = $this->createForm(MainForm::class, $mainFormEntity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            return $this->redirectToRoute('resume', ['user' => $task->getUserName()]);
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/resume/{user}", name="resume")
     */
    public function resumeAction(Request $request)
    {
        $userName = $request->get('user');
        $restService = $this->container->get('rest_client');
        try {
            $userInformation = $restService->userInformation($userName);
            $repositoryList = $restService->repositoryList($userName);
            $organizations = $restService->organizationList($userName);
            $languages = $this->getLanguages($restService, $repositoryList, $userName);
        } catch (BasicException $e) {
            return $this->render('default/resumeError.html.twig', [
                'message' => 'For userName we get error (' . $e->getMessage() . ')'
            ]);
        }

        return $this->render('default/resumeSuccess.html.twig', [
            'userName' => $userName,
            'userInformation' => $userInformation,
            'repositoryList' => $repositoryList,
            'organizations' => $organizations,
            'languages' => $languages
        ]);
    }

    private function getLanguages($restService, $repositoryList, $userName)
    {
        $languages = [];
        $sum =0;
        foreach ($repositoryList as $repository) {
            $simpleLang = (array) $restService->languageList($userName, $repository->name);
            foreach ($simpleLang as $lang => $value){
                $sum += $value;
                if (isset($languages[$lang])) {
                    $languages[$lang] += $value;
                } else {
                    $languages[$lang] = $value;
                }
            }
        }
        arsort($languages);
        foreach ($languages as $key => $value) {
            $percent = ($value / $sum) * 100;
            $languages[$key] = round($percent, 2);
        }
        return $languages;
    }

}
