<?php
namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;

class MainFormEntity
{
    /**
     * @Assert\NotBlank()
     * @var String
     */
    private $userName;

    /**
     * @return String
     */
    public function getUserName(): ?String
    {
        return $this->userName;
    }

    /**
     * @param String $userName
     */
    public function setUserName(String $userName)
    {
        $this->userName = $userName;
    }
}
