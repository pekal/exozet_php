<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Github', $crawler->filter('#container h1')->text());
    }

    public function testResume()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/resume/user');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('user', $crawler->filter('#container h1')->text());
    }
}
